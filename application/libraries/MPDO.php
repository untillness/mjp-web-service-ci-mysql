<?php

class MPDO
{

    private $PDO, $CI;

    protected $resultDataGetWhere, $resultDataWhere, $resultDataGet, $resultDataInsert;

    public function __construct()
    {
        $this->CI = &get_instance();

        $this->CI->load->database();
        $dbconfig = $this->CI->db;

        $host = $dbconfig->hostname;
        $dbname = $dbconfig->database;
        $username = $dbconfig->username;
        $password = $dbconfig->password;

        $this->PDO = new PDO("mysql:host={$host};dbname={$dbname}", $username, $password);
        $this->PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function insert($table, $data)
    {
        $query = 'INSERT INTO ';
        $query .= $table . ' (';
        $addParam = '';

        foreach ($data as $key => $val) {
            end($data);
            $currentKey = key($data);
            $addComma = $key != $currentKey ? ', ' : '';
            $query .= $key . $addComma;
            $addParam .= ':' . $key . $addComma;
        }

        $query .= ') VALUES (' . $addParam . ')';

        $db = $this->PDO->prepare($query);

        $itteration = 1;
        foreach ($data as $key => $val) {
            $db->bindValue(':' . $key, $val);
            $itteration++;
        }

        try {
            $db->execute();
            $id = $this->PDO->lastInsertId();
            return $id;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function delete($table, $data = null)
    {
        $query = 'DELETE FROM ';
        $query .= $table;
        $addParam = '';

        if ($data != null) {
            $query .= ' WHERE  ';
            foreach ($data as $key => $val) {
                end($data);
                $currentKey = key($data);
                $addAND = $key != $currentKey ? ' AND ' : '';
                $query .= $key . ' = :' . $key . $addAND;
                $addParam .= ':' . $key . $addAND;
            }
        }

        $db = $this->PDO->prepare($query);

        $itteration = 1;
        foreach ($data as $key => $val) {
            $db->bindValue(':' . $key, $val);
            $itteration++;
        }
        try {
            $db->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function get_where($table, $data)
    {
        $query = 'SELECT * FROM ';
        $query .= $table . ' WHERE ';
        $addParam = '';

        foreach ($data as $key => $val) {
            end($data);
            $currentKey = key($data);
            $addAND = $key != $currentKey ? ' AND ' : '';
            $query .= $key . ' = :' . $key . $addAND;
            $addParam .= ':' . $key . $addAND;
        }

        $db = $this->PDO->prepare($query);

        $itteration = 1;
        foreach ($data as $key => $val) {
            $db->bindValue(':' . $key, $val);
            $itteration++;
        }

        try {
            $db->execute();
            $this->resultDataGetWhere = $db;
            return $this;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function count()
    {
        $query = $this->resultDataGetWhere;
        $this->resultDataGetWhere = null;
        return $query->rowCount();
    }

    public function where($col, $val)
    {
        $expolodeWhere = explode('WHERE', $this->resultDataWhere);
        $query = count($expolodeWhere) > 1 ?  ' AND ' . $col . '=' . $val : 'WHERE ' . $col . '=' . $val;
        $this->resultDataWhere .= $query;
    }

    public function get($table)
    {
        $query = 'SELECT * FROM ' . $table . ' ' . $this->resultDataWhere;

        $db = $this->PDO->prepare($query);

        try {
            $db->execute();
            $this->resultDataWhere = null;
            $this->resultDataGet = $db;
            return $this;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function result()
    {
        return $this->resultDataGet->fetchAll(PDO::FETCH_ASSOC);
    }

    public function first()
    {
        $getQuery = [
            $this->resultDataGetWhere,
            $this->resultDataGet
        ];


        $pilihQuery = '';
        foreach ($getQuery as $query) {
            if ($query == null) {
                continue;
            }
            $pilihQuery = $query;
            break;
        }
        $this->resultDataGetWhere = null;
        $this->resultDataGet = null;
        return $pilihQuery->fetch(PDO::FETCH_ASSOC);
    }

    public function update($table, $data)
    {
        $query = 'UPDATE ';
        $query .= $table . ' SET ';
        $addParam = '';

        foreach ($data as $key => $val) {
            if ($key == 'id') {
                continue;
            }
            end($data);
            $currentKey = key($data);
            $addComma = $key != $currentKey ? ', ' : '';
            $addParam .= $key . '=:' . $key . $addComma;
        }
        $query .= $addParam;

        $query .= ' ' . $this->resultDataWhere;

        $db = $this->PDO->prepare($query);

        $itteration = 1;
        foreach ($data as $key => $val) {
            if ($key == 'id') {
                continue;
            }
            $db->bindValue(':' . $key, $val);
            $itteration++;
        }

        try {
            $db->execute();
            return true;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}
