<?php

class MSQL
{

    private $MSQL, $CI;

    protected $resultDataGetWhere, $resultDataWhere, $resultDataGet, $resultDataInsert;

    public function __construct()
    {
        $this->CI = &get_instance();

        $this->CI->load->database();
        $dbconfig = $this->CI->db;

        $host = $dbconfig->hostname;
        $dbname = $dbconfig->database;
        $username = $dbconfig->username;
        $password = $dbconfig->password;

        $this->MSQL = new mysqli($host, $username, $password, $dbname);
    }

    public function insert($table, $data)
    {
        $query = 'INSERT INTO ';
        $query .= $table . ' (';
        $addParam = '';

        foreach ($data as $key => $val) {
            end($data);
            $currentKey = key($data);
            $addComma = $key != $currentKey ? ', ' : '';
            $query .= $key  . $addComma;
            $addParam .= '"' . $val . '"' . $addComma;
        }

        $query .= ') VALUES (' . $addParam . ')';

        $db = $this->MSQL->query($query);

        return $this->MSQL->insert_id;
    }

    public function delete($table, $data = null)
    {
        $query = 'DELETE FROM ';
        $query .= $table;
        $addParam = '';

        if ($data != null) {
            $query .= ' WHERE  ';
            foreach ($data as $key => $val) {
                end($data);
                $currentKey = key($data);
                $addAND = $key != $currentKey ? ' AND ' : '';
                $query .= $key . ' = ' . $val . '' . $addAND;
            }
        }

        $db = $this->MSQL->query($query);
    }

    public function get_where($table, $data)
    {
        $query = 'SELECT * FROM ';
        $query .= $table . ' WHERE ';
        $addParam = '';

        foreach ($data as $key => $val) {
            end($data);
            $currentKey = key($data);
            $addAND = $key != $currentKey ? ' AND ' : '';
            $query .= $key . ' = "' . $val . '"' . $addAND;
            $addParam .= $val . $addAND;
        }


        $db = $this->MSQL->query($query);
        $this->resultDataGetWhere = $db;
        return $this;
    }

    public function count()
    {
        $query = $this->resultDataGetWhere;
        $this->resultDataGetWhere = null;
        return $query->num_rows;
    }

    public function where($col, $val)
    {
        $expolodeWhere = explode('WHERE', $this->resultDataWhere);
        $query = count($expolodeWhere) > 1 ?  ' AND ' . $col . '=' . $val : 'WHERE ' . $col . '=' . $val;

        $this->resultDataWhere .= $query;
    }

    public function get($table)
    {
        $query = 'SELECT * FROM ' . $table . ' ' . $this->resultDataWhere;

        $db = $this->MSQL->query($query);
        $this->resultDataWhere = null;
        $this->resultDataGet = $db;
        return $this;
    }

    public function result()
    {
        $data = [];
        while ($res = $this->resultDataGet->fetch_assoc()) {
            $data[] = $res;
        }
        return $data;
    }

    public function first()
    {
        $getQuery = [
            $this->resultDataGetWhere,
            $this->resultDataGet
        ];


        $pilihQuery = '';
        foreach ($getQuery as $query) {
            if ($query == null) {
                continue;
            }
            $pilihQuery = $query;
            break;
        }
        $this->resultDataGetWhere = null;
        $this->resultDataGet = null;
        return $pilihQuery->fetch_assoc();
    }

    public function update($table, $data)
    {
        $query = 'UPDATE ';
        $query .= $table . ' SET ';
        $addParam = '';

        foreach ($data as $key => $val) {
            if ($key == 'id') {
                continue;
            }
            end($data);
            $currentKey = key($data);
            $addComma = $key != $currentKey ? ', ' : '';
            $addParam .= $key . '= "' . $val . '"' . $addComma;
        }
        $query .= $addParam;

        $query .= ' ' . $this->resultDataWhere;

        $db = $this->MSQL->query($query);
    }
}
