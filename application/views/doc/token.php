<div id="content">
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="section-judul">Api Token</h3>
                </div>
                <div class="col-lg-9 col-sm-10 col-10">
                    <p>Your Token:</p>
                </div>
                <div class="col-lg-3 col-sm-2 col-2">
                    <button id="copy" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="token copied!">copy</button>
                </div>
                <div class="col-lg-12">
                    <p id="token" class="token p-2 pt-3"><?= $token ?></p>
                </div>

                <div class="col-12">
                    <p>
                        Use this TOKEN API to identify that you are.
                        For use, enter the TOKEN API into the header with the name <code>X-Auth</code>
                    </p>
                </div>
            </div>
        </div>
    </section>
</div>