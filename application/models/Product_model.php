<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product_model extends CI_Model
{
    private $table = 'products';

    function getProduk($id, $idUser)
    {
        $this->msql->where('id_user', $idUser);
        if ($id == null) {
            $product = $this->msql->get($this->table)->result();
        } else {
            $this->msql->where('id', $id);
            $product = $this->msql->get($this->table)->first();
        }
        return $product;
    }

    public function insertProduk($data)
    {
        $this->msql->insert($this->table, $data);
    }

    public function cekId($id, $idUser)
    {
        $data = $this->msql->get_where($this->table, ['id' => $id, 'id_user' => $idUser])->count();
        return $data;
    }

    public function updateProduct($id, $data)
    {
        $this->msql->where('id', $id);
        $this->msql->update($this->table, $data);
    }

    public function deleteProduct($id, $idUser)
    {
        $this->msql->delete($this->table, [
            'id' => $id,
            'id_user' => $idUser
        ]);
    }
}
